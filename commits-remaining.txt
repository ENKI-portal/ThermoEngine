[ ] + 3f139a03 Remove License scanning from .gitlab-ci.yml due to removal from GitLab
  - Maybe later?
 

[x] Squash all changes into single commit via git diff f1602cfb ed2ee36b | git apply --3way
   [ ] + ed2ee36b Fix pipeline- remove pybuild job
   [ ] + 86999bdc Adjust sudo flags for pybuild job CI pipeline
   [ ] + 3083c4a0 Add CI passwd to sudo cmd in pybuild  job
   [ ] + dd447ddb Add sudo permissions to gitlab CI pybuild job
   [ ] + 216a80bd remove gitlab-ci change trigger for build runtime
   [ ] + a1493f40 Remove pipeline yml trigger for build jobs
   [ ] + 3266f91a Disable pyinstall job rules while debugging
   
   # Just a pipeline trigger
   [-] + ae62ce70 Test explicit pip install CI pipeline
   [ ] + 2a7077fe Debug pybuild in CI pipeline
   
   # Just a pipeline trigger
   [-] + 5d204b4f Test pyinstall permissions in ci pipeline
   
   [ ] + 2f07e365 Try to fix permissions on py build
   [ ] + a1e95f02 Test Py-build stage of CI pipeline
   [ ] + 2cc162cc Add rules for py-build CI pipeline job
   [ ] + fa6e5d95 Add merge request to rules for CI build pipeline
   
   [ ] + bcd82763 Debug combined only/rules for CI pipeline
   [ ] + 7a5ccaa2 Add rules to build runtime
   [ ] + 01f8efb3 Correct CI pipeline error (syntax)
   [ ] + ac840f35 Add py-build stage to CI pipeline
   
   [ ] + e909920c Delete deactivated tests in CI pipeline
   
   # Ignore changes made and unmade
   [-] + 41984fcb Undo name change in pipeline
   [-] + 0450dccd Rename test pipeline secure




[x] + f1602cfb Remove redundant NB tests in pipeline
    - currently removes direct-tests in test-pkg stage (redundant with run-all-tests in test-production)
    - could combine into single 

[x] - Need to switch to staging in run-docker-locally, since that is being used in testing in CI pipeline


# Remove changes made and fully undone
[-] + 80f39495 Revert changes to pipeline
[-] + 68b2263f streamline CI pipeline - remove redundant tests - reorder jobs

[x] + 22bde2a1 Add make devinstall option
[x] + e3040319 Update local docker setup to use staging image
