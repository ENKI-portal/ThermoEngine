#!/bin/bash
MASTER_CONTAINER_NAME="registry.gitlab.com/enki-portal/thermoengine:master"
STAGING_CONTAINER_NAME="registry.gitlab.com/enki-portal/thermoengine:staging"
CONTAINER_NAME=$STAGING_CONTAINER_NAME
# CONTAINER_NAME=$MASTER_CONTAINER_NAME
if [ -z "$1" ]
then 
	echo "Accessing a Jupyter Lab ThermoEngine container rooted to this directory ..."
	docker pull $CONTAINER_NAME
	docker run -p 8888:8888 \
               --env JUPYTER_ENABLE_LAB=yes \
               --user root \
               -e GRANT_SUDO=yes \
               -v $PWD:/home/jovyan/ThermoEngine \
               $CONTAINER_NAME start-notebook.sh
 elif [ "$1" = "term" ]
 then
 	echo "Running an interactive shell into a ThermoEngine container ..."
 	docker pull $CONTAINER_NAME
    docker run --user root \
               -it \
               -v $PWD:/home/jovyan/ThermoEngine \
               --rm $CONTAINER_NAME bash
 elif [ "$1" = "stop" ]
 then
 	echo "Removing running docker containers ..."
 	if [ ! -z "$(docker ps -q)" ]
 	then
 		docker kill $(docker ps -q)
 	fi
 else
 	echo "Usage:"
 	echo "  <no argument> - Run latest ThermoEngine docker container from GitLab."
 	echo "  term - Run a terminal shell in the latest ThermoEngine docker container from GitLab."
 	echo "  stop - Remove any running docker containers from your system."
 	echo "  help - This message."
 fi
