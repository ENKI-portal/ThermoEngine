
const char *Forsterite_stixrude_tmp_identifier(void);
const char *Forsterite_stixrude_tmp_name(void);
const char *Forsterite_stixrude_tmp_formula(void);
const double Forsterite_stixrude_tmp_mw(void);
const double *Forsterite_stixrude_tmp_elements(void);

double Forsterite_stixrude_tmp_g(double T, double P);
double Forsterite_stixrude_tmp_dgdt(double T, double P);
double Forsterite_stixrude_tmp_dgdp(double T, double P);
double Forsterite_stixrude_tmp_d2gdt2(double T, double P);
double Forsterite_stixrude_tmp_d2gdtdp(double T, double P);
double Forsterite_stixrude_tmp_d2gdp2(double T, double P);
double Forsterite_stixrude_tmp_d3gdt3(double T, double P);
double Forsterite_stixrude_tmp_d3gdt2dp(double T, double P);
double Forsterite_stixrude_tmp_d3gdtdp2(double T, double P);
double Forsterite_stixrude_tmp_d3gdp3(double T, double P);

double Forsterite_stixrude_tmp_s(double T, double P);
double Forsterite_stixrude_tmp_v(double T, double P);
double Forsterite_stixrude_tmp_cv(double T, double P);
double Forsterite_stixrude_tmp_cp(double T, double P);
double Forsterite_stixrude_tmp_dcpdt(double T, double P);
double Forsterite_stixrude_tmp_alpha(double T, double P);
double Forsterite_stixrude_tmp_beta(double T, double P);
double Forsterite_stixrude_tmp_K(double T, double P);
double Forsterite_stixrude_tmp_Kp(double T, double P);

